import argparse
import os
import subprocess
import json
import traceback

from ost import io
from ost import conop
from ost.mol.alg.chain_mapping import ChainMapper
from ost.mol.alg.qsscore import QSScorer
from ost.io import ReadStereoChemicalPropsFile
from ost.mol.alg import CheckStructure, Molck, MolckSettings

def _parse_args():
    desc = ("Computes QS-scores based on chain mappings from MMalign as "
            "computed by the predictioncenter. You need to run the following "
            "data collection scripts as described in README: "
            "collect_assemblies.py, collect_targets.py and "
            "collect_mmalign_results.py. Many of the scores will be None."
            "Reason for that are 1) models with empty chain names, "
            "2) Models with chains where the residue numbers are not strictly "
            "increasing (requirenent of ChainMapper), 3) MMalign maps chains "
            "with non-equal sequences. 3 is what happens most often.") 
    parser = argparse.ArgumentParser(description = desc)
    parser.add_argument("model_dir", help = "Path to directory with assembly "
                        "models (out_dir argument in collect_assemblies.py)")
    parser.add_argument("target_dir", help="Path to directory with assembly "
                        "targets (out_dir argument in "
                        "collect_assembly_targets.py)")
    parser.add_argument("mmalign_summary_dir", help="Path to MMalign "
                        "summaries as computed by the predictioncenter "
                        "(out_dir argument in collect_mmalign_results.py)")
    parser.add_argument("target_file", help="JSON file that relates targets "
                        "with actual target filenames. Sits in <GIT_ROOT>")
    parser.add_argument("out_file", help="Output data in JSON format")
    return parser.parse_args() 

class MMalignResult:
    """ helper object to hold MMalign results for a single model

    :param trg_filename: Name of PDB file with target coordinates
    :type trg_filename: :class:`str`
    :param mdl_filename: Name of PDB file with model coordinates
    :type mdl_filename: :class:`str`
    :param tmscore: TM-score as computed by MMalign
    :type tmscore: :class:`float`
    :param flat_mapping: Dictionary with target chain names as key and the
                         mapped mdl chain names as value. Only mapped pairs!
                         Non mapped chains can be left out.
    :type flat_mapping: :class:`dict` with :class:`str` as key/value
    """
    def __init__(self, trg_filename, mdl_filename, tmscore, flat_mapping):
        self.trg_filename = trg_filename
        self.mdl_filename = mdl_filename
        self.tmscore = tmscore
        self.flat_mapping = flat_mapping

    @staticmethod
    def FromSummaryLine(line):
        """ Static function to create object of type MMAlignResult from line in
        predictioncenter SUMMARY file

        :param line: Single line of MMalign SUMMARY file
        :type line: :class:`str`
        """
        data = line.strip().split()
        mdl_filename = data[0]
        try:
            tmscore = float(data[1])
        except:
            print(line)
            raise
        mdl_mapping_str = data[2]
        assert(mdl_mapping_str.startswith(mdl_filename))
        trg_mapping_str = data[3]
        assert(mdl_mapping_str[:5] == trg_mapping_str[:5])
        split_mdl_mapping_str = mdl_mapping_str.split(':')
        split_trg_mapping_str = trg_mapping_str.split(':')
        assert(len(split_mdl_mapping_str) > 0 and \
               len(split_mdl_mapping_str) == len(split_trg_mapping_str))
        flat_mapping = dict()
        trg_filename = split_trg_mapping_str[0]
        for trg_ch, mdl_ch in zip(split_trg_mapping_str[1:],
                                  split_mdl_mapping_str[1:]):
            if trg_ch != '' and mdl_ch != '':
                # USalign result from the predictioncenter come as
                # 1,A instead of A
                if ',' in trg_ch and ',' in mdl_ch:
                    flat_mapping[trg_ch.split(',')[1]] = mdl_ch.split(',')[1]
                else:
                    flat_mapping[trg_ch] = mdl_ch
        return MMalignResult(trg_filename, mdl_filename, tmscore, flat_mapping)

def _parse_summary_file(sf_path):
    """ Parses an MMalignResult object for every line in sf_path
    """
    with open(sf_path, 'r') as fh:
        data = fh.readlines()
    # only parse lines with correct number of elements
    data = [line for line in data if len(line.split()) == 4]
    return [MMalignResult.FromSummaryLine(line) for line in data]

def _process_model(trg, mdl, mmalign_result):
    """ Computes QS-score for one single model

    Has grown into a function to identify all the weirdness we get
    from CASP models...
    Returns a dict with keys "score", "errors" and "n_atoms_removed".
    If any error occurs, score and n_atoms_removed will be None and
    errors are listed in "errors"

    Currently we're dealing with:

    - empty chain names
    - requirement of the ChainMapper: residue numbers in a chain must be
      strictly increasing
    - mappings of mmalign which are invalid, i.e. mapping between chains
      with different sequences... well, in the end thats what MMalign is
      supposed to do, sequence independent superposition. But in this case
      we should filter it out anyways.
    """

    # do cleaning on model, lets wait with stereochecks for after the
    # alignment tests
    ms = MolckSettings(rm_unk_atoms=True,
                       rm_non_std=True,
                       rm_hyd_atoms=True,
                       rm_oxt_atoms=True,
                       rm_zero_occ_atoms=False,
                       colored=False,
                       map_nonstd_res=True,
                       assign_elem=True)
    Molck(mdl, conop.GetDefaultLib(), ms)
    mdl = mdl.CreateFullView()

    # catch models which have empty chain names
    empty_ch = False
    for ch in mdl.chains:
        if ch.GetName().strip() == "":
            empty_ch = True
            break
    if empty_ch:
        return {"QS_global": None,
                "QS_best": None,
                "tm_score": mmalign_result.tmscore,
                "errors": "empty chain name observed"}

    # check whether the flat mapping from mmalign is actually valid,
    # i.e. whether only chains with equal sequence are mapped together
    mapper = ChainMapper(trg, resnum_alignments=True)
    chem_groups = mapper.chem_groups
    trg_chem_group_mapper = dict()
    for chem_group_idx, chem_group in enumerate(chem_groups):
        for cname in chem_group:
            trg_chem_group_mapper[cname] = chem_group_idx
    try:
        chem_mapping, _, _ = mapper.GetChemMapping(mdl)
    except RuntimeError as e:
        if str(e).startswith("Residue numbers in input structures must be "):
            return {"QS_global": None,
                    "QS_best": None,
                    "tm_score": mmalign_result.tmscore,
                    "errors": ["Model does not fulfull requirement of "
                               "strictly increasing residue numbers in "
                               "each chain"]}
        else:
            raise
    mdl_chem_group_mapper = dict()
    for chem_group_idx, mdl_chem_group in enumerate(chem_mapping):
        for cname in mdl_chem_group:
            mdl_chem_group_mapper[cname] = chem_group_idx

    mismatches = list()
    for k,v in mmalign_result.flat_mapping.items():
        if k not in trg_chem_group_mapper or v not in mdl_chem_group_mapper:
            return {"QS_global": None,
                    "QS_best": None,
                    "tm_score": mmalign_result.tmscore,
                    "errors": [f"Trg ch {k} or mdl ch {v} not present... "
                               f"possibly removed because of terrible stereo-"
                               f"chemistry?"]}
        if trg_chem_group_mapper[k] != mdl_chem_group_mapper[v]:
            mismatches.append((k, v))

    if len(mismatches) > 0:
        return {"QS_global": None,
                "QS_best": None,
                "tm_score": mmalign_result.tmscore,
                "errors": [f"invalid chain mapping (trg {x[0]}, mdl {x[1]})" \
                           for x in mismatches]}

    # make sure that we only have residues with all required backbone atoms
    trg, _, _ = mapper.ProcessStructure(trg)
    mdl, _, _ = mapper.ProcessStructure(mdl)

    # setup QSScorer and score
    alns = dict()
    remove_from_flat_mapping = list()
    flat_mapping = dict(mmalign_result.flat_mapping)
    for k, v in flat_mapping.items():
        trg_ch = trg.Select(f"cname={k}")
        mdl_ch = mdl.Select(f"cname={v}")
        if len(trg_ch.residues) == 0 or len(mdl_ch.residues) == 0:
            remove_from_flat_mapping.append(k)
            continue
        trg_s = ''.join([r.one_letter_code for r in trg_ch.residues])
        trg_s = seq.CreateSequence(k, trg_s)
        trg_s.AttachView(trg_ch)
        mdl_s = ''.join([r.one_letter_code for r in mdl_ch.residues])
        mdl_s = seq.CreateSequence(v, mdl_s)
        mdl_s.AttachView(mdl_ch)
        aln = mapper.Align(trg_s, mdl_s, mol.ChemType.AMINOACIDS)
        alns[(k,v)] = aln

    for k in remove_from_flat_mapping:
        del flat_mapping[k]

    qs_scorer = QSScorer(trg, mapper.chem_groups, mdl, alns)
    score_result = qs_scorer.FromFlatMapping(flat_mapping)

    return {"QS_global": score_result.QS_global,
            "QS_best": score_result.QS_best,
            "tm_score": mmalign_result.tmscore,
            "errors": list()}

def _process_summary_file(trg, trg_filename, sf_path, model_dir, target_dir):
    mmalign_data = _parse_summary_file(sf_path)
    trg_ent = io.LoadPDB(os.path.join(target_dir, trg_filename))

    # do cleaning and stereochemistry checks on target
    ms = MolckSettings(rm_unk_atoms=True,
                       rm_non_std=True,
                       rm_hyd_atoms=True,
                       rm_oxt_atoms=True,
                       rm_zero_occ_atoms=False,
                       colored=False,
                       map_nonstd_res=True,
                       assign_elem=True)
    Molck(trg_ent, conop.GetDefaultLib(), ms)
    stereo_param = ReadStereoChemicalPropsFile()
    trg_ent = trg_ent.CreateFullView()

    results = dict()
    for d in mmalign_data:
        print("processing:", d.mdl_filename)
        try:
            mdl_path = os.path.join(model_dir, trg, d.mdl_filename)
            mdl_ent = io.LoadPDB(mdl_path)
        except:
            print(f"Could not load model: {mdl_path}")
            continue
        results[d.mdl_filename] = _process_model(trg_ent, mdl_ent, d)
    return results

def main():
    args = _parse_args()
    with open(args.target_file) as fh:
        target_data = json.load(fh)
    results = dict()
    for trg, trg_files in target_data.items():
        # only take first target...
        trg_filename = trg_files[0]
        sf_path = os.path.join(args.mmalign_summary_dir,
                               trg_filename.replace(".pdb", ".SUMMARY"))
        results[trg] = _process_summary_file(trg, trg_filename, sf_path,
                                             args.model_dir, args.target_dir)
    with open(args.out_file, 'w') as fh:
        json.dump(results, fh)

if __name__ == '__main__':
    main()
