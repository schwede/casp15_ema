import argparse
import os
import json

def _parse_args():

    desc = ("Generates a file with commands that execute <x>_scoring.py"
            "for each model. <x> stands for global or local")
    parser = argparse.ArgumentParser(description = desc)
    parser.add_argument("ema_exec", help="path to ema executable")
    parser.add_argument("oligo_scoring_script", help="path to "
                        "<x>_scoring.py script")
    parser.add_argument("mdl_dir", help="Directory containing a directory "
                        "with models for each target.")
    parser.add_argument("trg_dir", help="Directory with target structures")
    parser.add_argument("target_file", help="JSON file that relates targets "
                        "with actual target filenames")
    parser.add_argument("result_dir", help="directory where "
                        "oligo_scoring_script dumps result files - "
                        "one for each model.")
    parser.add_argument("out", help="outfile with created commands")
    return parser.parse_args()

def main():
    args = _parse_args()
    commands = list()
    with open(args.target_file) as fh:
        target_data = json.load(fh)

    for target, target_files in target_data.items():

        mdl_dir = os.path.join(args.mdl_dir, target)
        if not os.path.isdir(mdl_dir):
            raise RuntimeError(f"Expect model directory at {mdl_dir}")

        model_files = os.listdir(mdl_dir)

        if target in ["H1171", "H1172"]:
            # special case with several reference structures...
            tfs = target_files
        else:
            tfs = [target_files[0]]

        for tf in tfs:
            trg_path = os.path.join(args.trg_dir, tf)
            if not os.path.isfile(trg_path):
                raise RuntimeError(f"Expect target structure at {trg_path}")

            for mf in model_files:
                mdl_path = os.path.join(mdl_dir, mf)
                tmp = mf + "_" + tf.split('.')[0] + ".json"
                result_path = os.path.join(args.result_dir, tmp)
                cmd = [args.ema_exec,
                       args.oligo_scoring_script,
                       mdl_path, trg_path, result_path]
                commands.append(' '.join(cmd))

    with open(args.out, 'w') as fh:
        fh.write(os.linesep.join(commands))

if __name__ == '__main__':
    main()
