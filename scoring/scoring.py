import argparse
import os
import json
import time
import sys

from ost import io
from ost.mol.alg import scoring

def _parse_args():

    desc = ("Computes scores and chain mapping info required for CASP15 EMA "
            "category (and more).") 

    parser = argparse.ArgumentParser(description = desc)
    parser.add_argument("mdl_file", help="model file in PDB format.")
    parser.add_argument("trg_file", help="target file in PDB format")
    parser.add_argument("out_file", help="Output data in JSON format")
    return parser.parse_args()

def _crash_out(error, out_path):
    """ Produces output JSON containing error and exits with return code 1
    """
    with open(out_path, 'w') as fh:
        json.dump({"errors": [error]}, fh)
    sys.exit(1)

def _local_scores_to_json_dict(score_dict):
    """ Convert ResNums to str for JSON serialization
    """
    json_dict = dict()
    for ch, ch_scores in score_dict.items():
        for num, s in ch_scores.items():
            ins_code = num.ins_code.strip("\u0000")
            json_dict[f"{ch}.{num.num}.{ins_code}"] = s
    return json_dict

def _patch_scores_to_json_list(interface_dict, score_dict):
    """ Creates List of patch scores that are consistent with interface residue
    lists
    """
    json_list = list()
    for ch, ch_nums in interface_dict.items():
        json_list += score_dict[ch]
    return json_list

def _interface_residues_to_json_list(interface_dict):
    """ Convert ResNums to str for JSON serialization.

    Changes in this function will affect _PatchScoresToJSONList
    """
    json_list = list()
    for ch, ch_nums in interface_dict.items():
        for num in ch_nums:
            ins_code = num.ins_code.strip("\u0000")
            json_list.append(f"{ch}.{num.num}.{ins_code}")
    return json_list

def main():


    t0 = time.time()
    args = _parse_args()

    trg = io.LoadPDB(args.trg_file)
    try:
        mdl = io.LoadPDB(args.mdl_file)
    except Exception as e:
        if str(e).startswith("duplicate atom 'OXT' in residue "):
            _crash_out(str(e), args.out_file)
        else:
            raise

    try:
        scorer = scorer = scoring.Scorer(mdl, trg, resnum_alignments=True)
    except Exception as e:       
        _crash_out(str(e), args.out_file)

    data = dict()
    try:
        data["trg_file"] = args.trg_file
        data["mdl_file"] = args.mdl_file
        data["qs_global"] = round(scorer.qs_global, 3)
        data["qs_best"] = round(scorer.qs_best, 3)
        data["gdtts"] = round(scorer.gdtts, 3)
        data["gdtts_transform"] = scorer.transform.data
        data["rmsd"] = round(scorer.rmsd, 3)
        data["dockq_ave"] = scorer.dockq_ave
        data["dockq_wave"] = scorer.dockq_wave
        data["dockq_ave_full"] = scorer.dockq_ave_full
        data["dockq_wave_full"] = scorer.dockq_wave_full
        data["lddt"] = round(scorer.lddt, 3)
        data["local_lddt"] = _local_scores_to_json_dict(scorer.local_lddt)
        data["cad_score"] = scorer.cad_score
        data["local_cad_score"] = _local_scores_to_json_dict(scorer.local_cad_score)
        N = len(scorer.model.atoms) - len(scorer.stereochecked_model.atoms)
        data["stereocheck_atoms_removed_mdl"] = N
        N = len(scorer.target.atoms) - len(scorer.stereochecked_target.atoms)
        data["stereocheck_atoms_removed_trg"] = N
        data["interface_residues"] = _interface_residues_to_json_list(scorer.model_interface_residues)
        data["target_interface_residues"] = _interface_residues_to_json_list(scorer.target_interface_residues)
        data["patch_qs"] = _patch_scores_to_json_list(scorer.model_interface_residues, scorer.patch_qs)
        data["patch_dockq"] = _patch_scores_to_json_list(scorer.model_interface_residues, scorer.patch_dockq)
        data["mapping"] = scorer.mapping.GetFlatMapping(mdl_as_key=True)
        data["runtime"] = time.time() - t0
    except Exception as e:       
        _crash_out(str(e), args.out_file)

    with open(args.out_file, 'w') as fh:
        json.dump(data, fh)

if __name__ == '__main__':
    main()
