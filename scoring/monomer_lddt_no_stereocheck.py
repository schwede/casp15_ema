import argparse
import os
import json

from ost import io
from ost.mol.alg.lddt import lDDTScorer

def _parse_args():
    desc = ("Computes global and per-residue lDDT score on monomer models "
            "without performing a stereo-chemistry check")
    parser = argparse.ArgumentParser(description = desc)
    parser.add_argument("mdl_file", help = "model file in PDB format")
    parser.add_argument("trg_file", help="target file in PDB format")
    parser.add_argument("out_file", help="Output data in JSON format")
    return parser.parse_args() 

def main():

    args = _parse_args()

    mdl = io.LoadPDB(args.mdl_file)        
    trg = io.LoadPDB(args.trg_file)

    assert(len(mdl.chains)==1)
    assert(len(trg.chains)==1)

    lddt_scorer = lDDTScorer(trg)
    lddt_score, _ = lddt_scorer.lDDT(mdl, local_lddt_prop="lddt")

    local_scores = dict()
    for r in mdl.residues:
        if r.HasProp("lddt"):
            local_scores[r.GetNumber().GetNum()] = r.GetFloatProp("lddt")
        else:
            local_scores[r.GetNumber().GetNum()] = None

    # create and dump output
    json_summary = {"mdl_file": args.mdl_file,
                    "trg_file": args.trg_file,
                    "lDDT": lddt_score,
                    "local_lDDT": local_scores}
    
    with open(args.out_file, 'w') as fh:
        json.dump(json_summary, fh)

if __name__ == '__main__':
    main()
