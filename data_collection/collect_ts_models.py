import argparse
import os
import subprocess
import shutil

def _parse_args():

    desc = ("Fetches all casp15 regular, i.e. tertiary structure predictions "
            "from predictioncenter.org. Each target is unpacked and all model "
            "files are dumped in a separate directory that is created in the "
            "specified out_dir.")

    parser = argparse.ArgumentParser(description = desc)
    parser.add_argument("out_dir", help="A directory for each target will be "
                        "created in here")
    return parser.parse_args() 

def main():
    args = _parse_args()

    if not os.path.exists(args.out_dir):
        os.makedirs(args.out_dir)

    targets = ["T1104",
               "T1105",
               "T1105v1",
               "T1106s1",
               "T1106s2",
               "T1109",
               "T1110",
               "T1112",
               "T1113",
               "T1114s1",
               "T1114s2",
               "T1114s3",
               "T1115",
               "T1118",
               "T1118v1",
               "T1119",
               "T1120",
               "T1121",
               "T1122",
               "T1123",
               "T1124",
               "T1125",
               "T1127",
               "T1127v2",
               "T1129s2",
               "T1130",
               "T1131",
               "T1132",
               "T1133",
               "T1134s1",
               "T1134s2",
               "T1137s1",
               "T1137s2",
               "T1137s3",
               "T1137s4",
               "T1137s5",
               "T1137s6",
               "T1137s7",
               "T1137s8",
               "T1137s9",
               "T1139",
               "T1145",
               "T1146",
               "T1147",
               "T1150",
               "T1151s2",
               "T1152",
               "T1153",
               "T1154",
               "T1155",
               "T1157s1",
               "T1157s2",
               "T1158",
               "T1158v1",
               "T1158v2",
               "T1158v3",
               "T1158v4",
               "T1159",
               "T1160",
               "T1161",
               "T1162",
               "T1163",
               "T1165", 
               "T1169", 
               "T1170",
               "T1173",
               "T1174",
               "T1175",
               "T1176",
               "T1177",
               "T1178",
               "T1179",
               "T1180",
               "T1181",
               "T1182",
               "T1183",
               "T1184",
               "T1185s1", 
               "T1185s2",
               "T1185s3",
               "T1185s4",
               "T1186",
               "T1187",
               "T1188",
               "T1189",
               "T1190",
               "T1191",
               "T1192",
               "T1193",
               "T1194",
               "T1195",
               "T1196",
               "T1197"]

    for t in targets:
        url = f"https://predictioncenter.org/download_area/CASP15/predictions/"
        url += f"regular/{t}.tar.gz"
        out_path = os.path.join(args.out_dir, t + ".tar.gz")
        # download
        subprocess.run(["curl", url, "--output", out_path])
        # unpack
        subprocess.run(["tar", "-xvf", out_path, "--directory", args.out_dir])
        # delete tarball
        os.remove(out_path)

    # extra models as trg_name and file_names
    # -> stuff added after tar balls generated...
    extra_models = [
        ("T1109", ['T1109TS270_1', 'T1109TS270_2', 'T1109TS270_3',
                   'T1109TS270_4', 'T1109TS270_5']),
        ("T1110", ['T1110TS270_1', 'T1110TS270_2', 'T1110TS270_3',
                   'T1110TS270_4', 'T1110TS270_5']),
        ("T1113", ['T1113TS270_1', 'T1113TS270_2', 'T1113TS270_3',
                   'T1113TS270_4', 'T1113TS270_5']),
    ]
    pdb_base_url = "https://predictioncenter.org/casp15/MODELS_PDB"
    # fetch'em
    for trg_name, file_names in extra_models:
        for file_name in file_names:
            # note: store w/o PDB ending!
            url = os.path.join(pdb_base_url, trg_name, file_name + ".pdb")
            out_path = os.path.join(args.out_dir, trg_name, file_name)
            subprocess.run(["curl", url, "--output", out_path])

if __name__ == '__main__':
    main()
