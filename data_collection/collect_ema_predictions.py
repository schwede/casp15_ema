import argparse
import os
import subprocess
import shutil
import json

def _parse_args():

    desc = ("Fetches all casp15 ema predictions from "
            "predictioncenter.org. Files for each target are unpacked and "
            "dumped in separate directories that are created in the specified "
            "out_dir.")

    parser = argparse.ArgumentParser(description = desc)
    parser.add_argument("ema_target_json", help="Assembly target json "
                        "file in <GIT_ROOT>")
    parser.add_argument("out_dir", help="A directory for each target will be "
                        "created in here")
    return parser.parse_args() 

def main():
    args = _parse_args()

    if not os.path.exists(args.out_dir):
        os.makedirs(args.out_dir)

    with open(args.ema_target_json, 'r') as fh:
        target_data = json.load(fh)

    for target, target_files in target_data.items():
        print("processing", target)
        if os.path.exists(os.path.join(args.out_dir, target)):
            print(f"directory for {target} present... skip...")
            continue
        url = f"https://predictioncenter.org/download_area/CASP15/predictions/"
        url += f"QA/{target}.QA.tar.gz"
        print(f"download from {url}")
        out_path = os.path.join(args.out_dir, target + ".tar.gz")
        # download
        subprocess.run(["curl", url, "--output", out_path])
        # unpack
        subprocess.run(["tar", "-xvf", out_path, "--directory", args.out_dir])
        # delete tarball
        os.remove(out_path)

if __name__ == '__main__':
    main()

