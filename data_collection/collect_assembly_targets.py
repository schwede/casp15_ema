import argparse
import os
import subprocess
import json

def _parse_args():

    desc = ("Fetches all casp15 assembly targets from "
            "predictioncenter.org. Files are dumped in specified output "
            "directory. Files are password protected, login credentials and "
            "password must therefore be provided as arguments.")

    parser = argparse.ArgumentParser(description = desc)
    parser.add_argument("assembly_target_json", help="Assembly target json "
                        "file in <GIT_ROOT>")
    parser.add_argument("oligo_target_url", help = "Base url - target_file "
                        "x.pdb should reside at <oligo_target_url>/x.pdb")
    parser.add_argument("user", help="assessor user name")
    parser.add_argument("pw", help="assessor password")
    parser.add_argument("out_dir", help="Target files will be dumped here")
    return parser.parse_args() 

def main():
    args = _parse_args()

    if not os.path.exists(args.out_dir):
        os.makedirs(args.out_dir)

    with open(args.assembly_target_json, 'r') as fh:
        target_data = json.load(fh)

    for target, target_files in target_data.items():
        for tf in target_files:
            url = f"{args.oligo_target_url}/{tf}"
            out_path = os.path.join(args.out_dir, tf)
            subprocess.run(["curl", url, "-u", f"{args.user}:{args.pw}",
                            "--output", out_path])

if __name__ == '__main__':
    main()
