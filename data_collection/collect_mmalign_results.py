import argparse
import os
import subprocess
import json

def _parse_args():

    desc = ("Fetches mmalign summary files from predictioncenter.org. Files "
            "are dumped in specified output directory. Files are password "
            "protected, login credentials and password must therefore be "
            "provided as arguments.")

    parser = argparse.ArgumentParser(description = desc)
    parser.add_argument("mmalign_target_url", help = "Base url - target_file "
                        "x.SUMMARY should reside at "
                        "<mmalign_target_url>/x.SUMMARY")
    parser.add_argument("target_file", help="JSON file that relates targets "
                        "with actual target filenames. Sits in <GIT_ROOT>")
    parser.add_argument("user", help="assessor user name")
    parser.add_argument("pw", help="assessor password")
    parser.add_argument("out_dir", help="MMalign summary files will be dumped "
                        "here")
    return parser.parse_args() 

def main():
    args = _parse_args()

    if not os.path.exists(args.out_dir):
        os.makedirs(args.out_dir)

    with open(args.target_file) as fh:
        target_data = json.load(fh)

    for target, target_files in target_data.items():
        for tf in target_files:
            filename = tf.split('.')[0] + ".SUMMARY"
            url = f"{args.mmalign_target_url}/{filename}"
            out_path = os.path.join(args.out_dir, filename)
            subprocess.run(["curl", url, "-u", f"{args.user}:{args.pw}",
                            "--output", out_path])

if __name__ == '__main__':
    main()
