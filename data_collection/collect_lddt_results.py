import argparse
import os
import subprocess
import shutil

def _parse_args():

    desc = ("Fetches lDDT evaluations done for CASP15 from predictioncenter.org"
            " for all targets to be evaluated for self-assessment. Each target"
            " ends up in a subdirectory of the specified out_dir with a .lddt"
            " file for each evaluated model."
            " Note that this is a subset of the targets fetched by"
            " collect_ts_models and that for T1120 and T1170 we use the -D12"
            " evaluation (domain orientations were inconsistent between chains"
            " but it is good enough for an lDDT evaluation).")

    parser = argparse.ArgumentParser(description = desc)
    parser.add_argument("user", help="assessor user name")
    parser.add_argument("pw", help="assessor password")
    parser.add_argument("out_dir", help="A directory for each target will be "
                        "created in here")
    return parser.parse_args()

def main():
    # HC setup
    # -> download subfolders of lddt_url for each target
    lddt_url = "https://predictioncenter.org/casp15/assessors/RESULTS/LDDT"
    targets = [
      "T1104",
      "T1106s1",
      "T1106s2",
      "T1109",
      "T1110",
      "T1112",
      "T1113",
      "T1114s1",
      "T1114s2",
      "T1114s3",
      "T1115",
      "T1119",
      "T1120-D12",
      "T1121",
      "T1122",
      "T1123",
      "T1124",
      "T1125",
      "T1127",
      "T1129s2",
      "T1130",
      "T1131",
      "T1132",
      "T1133",
      "T1134s1",
      "T1134s2",
      "T1137s1",
      "T1137s2",
      "T1137s3",
      "T1137s4",
      "T1137s5",
      "T1137s6",
      "T1137s7",
      "T1137s8",
      "T1137s9",
      "T1139",
      "T1145",
      "T1146",
      "T1147",
      "T1150",
      "T1151s2",
      "T1152",
      "T1153",
      "T1154",
      "T1155",
      "T1157s1",
      "T1157s2",
      "T1158",
      "T1159",
      "T1160",
      "T1161",
      "T1162",
      "T1163",
      "T1165",
      "T1169",
      "T1170-D12",
      "T1173",
      "T1174",
      "T1175",
      "T1176",
      "T1177",
      "T1178",
      "T1179",
      "T1180",
      "T1181",
      "T1182",
      "T1183",
      "T1184",
      "T1185s1",
      "T1185s2",
      "T1185s4",
      "T1187",
      "T1188",
      "T1194",
      "T1195",
      "T1196",
      "T1197"
    ]

    args = _parse_args()

    if not os.path.exists(args.out_dir):
        os.makedirs(args.out_dir)

    for target in targets:
        # recursive download of .lddt files with wget
        dir_url = os.path.join(lddt_url, target)
        trg_dir = os.path.join(args.out_dir, target)
        subprocess.run(["wget", "-e", "robots=off", "-A", "lddt", "-r", "-np",
                        "-l", "1", "-nd", f"--user={args.user}",
                        f"--password={args.pw}", "-P", trg_dir, dir_url])

if __name__ == '__main__':
    main()
