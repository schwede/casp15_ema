# Estimation of Model Accuracy evaluation in CASP15 (EMA)

This repository aims for a reproducible analysis. 

Some files use git-lfs tracking (https://git-lfs.com/) which is required as a
dependency when cloning the repository. 

All scripts are executed in a
Singularity container (https://sylabs.io/). the `ema` executable helps to run
scripts or fire an Jupyter notebook for interactive analysis. Execute a script:
```bash
./ema my_scipt.py arg1 arg2
```
Fire a Jupyter notebook:
```bash
./ema --notebook
```
Print general help and advanced options:
```bash
./ema --help
```

For this to work, you first need to build a container... execute
`singularity build ema.sif Singularity` inside the container directory as root.
The container at that location will then be automatically picked up by the `ema`
script.

## Results

CASP15 EMA results derived with analysis steps described below are available
in various formats in `tables` directory.

## Targets

Targets are specified in two JSON formatted files located in <GIT_ROOT>:

* assembly_targets.json: All assembly targets evaluated in CASP 15,
  including special targets. Examples include certain substructures of
  interest or concatenated chains in antibodies to primarily focus on the
  interaction with the epitope.
  Formatted as dict, key: target name. A tarball with that name that
  contains the according models is expected to be available from
  https://predictioncenter.org/download_area/CASP15/predictions/oligo/.
  Value: a list of target structures. That exact files are expected to be
  available in the internal predictioncenter download section.
* ema_targets.json: Same for ema targets. Largely the same, except no
  special targets. Key: Tarball with according predictions is expected
  to be available from:
  https://predictioncenter.org/download_area/CASP15/predictions/QA/.
  Value: relevant target structures.

## Fetch Data

Data for the analysis resides in a directory `data` which is not tracked by git
(see .gitignore). To download and unpack all assembly models into directory
`data/assembly_models`, execute:

```bash
./ema data_collection/collect_assemblies.py assembly_targets.json data/assembly_models
```

The directory `data` will be created in the process.

Target files are password protected. The required url, user name and password
are therefore not provided in this repository and must be provided as arguments
to the respective download script:

```bash
./ema data_collection/collect_assembly_targets.py assembly_targets.json <data_url> <assessor_user> <assessor_pw> data/assembly_targets
```

TM-scores and the respective chain mappings computed with MMalign are directly
fetched from the predictioncenter website. Again, the results are password
protected:

```bash
./ema data_collection/collect_mmalign_results.py <data_url> assembly_targets.json <assessor_user> <assessor_pw> data/mm_align_results
```

Pro tip: USalign results can be fetched with the same script, just adapt data_url
and output directory - The final evaluation uses USalign results...

Tertiary structure models can be fetched with:
```bash
./ema data_collection/collect_ts_models.py data/ts_models
```

and their target sequences with:

```bash
./ema data_collection/collect_ts_target_sequences.py data/ts_target_sequences
```

and the results from their lDDT evaluations with:

```bash
./ema data_collection/collect_lddt_results.py <assessor_user> <assessor_pw> data/ts_lddt_results
```

To pull EMA predictions use:

```bash
./ema data_collection/collect_ema_predictions.py ema_targets.json data/ema_predictions
```

## Compute Scores

Create a directory to store intermediate results:

```bash
mkdir -p data/scores
```

Create a file with commands to compute scores for each model:

```bash
./ema scoring/create_scoring_cmds.py <GIT_ROOT>/ema <GIT_ROOT>/scoring/scoring.py <GIT_ROOT>/data/assembly_models/ <GIT_ROOT>/data/assembly_targets/ assembly_targets.json <GIT_ROOT>/data/scores data/scores/compute_scores.cmd
```

And submit to a compute cluster. The following commands are specific to the
sciCORE compute environment at the university of Basel, adapt to your
requirements.

create a stdout directory:

```bash
mkdir -p data/scores/stdout
```

and submit with following script, adapt where needed:

```bash
#!/bin/bash
#SBATCH --job-name=casp_scores
#SBATCH --qos=6hours
#SBATCH --output=<GIT_ROOT>/data/scores/stdout/%A_%a.out
#SBATCH --mem=12G
#SBATCH --array=1-11163

ml Python/3.6.6-foss-2018b

$(head -$SLURM_ARRAY_TASK_ID <GIT_ROOT>/data/scores/compute_scores.cmd | tail -1)
```


## Baseline Predictors

### Assembly Consensus

Create a directory to store intermediate results:

```bash
mkdir -p data/baselines/assembly_consensus
```

Create a file with commands to compute one vs. all chain mappings for each
assembly:

```bash
./ema baseline_predictors/assembly_consensus/create_one_vs_all_cmds.py <GIT_ROOT>/ema <GIT_ROOT>/baseline_predictors/assembly_consensus/one_vs_all_chain_mapping.py <GIT_ROOT>/data/assembly_models <GIT_ROOT>/data/baselines/assembly_consensus <GIT_ROOT>/data/baselines/assembly_consensus/one_vs_all.cmd
```
And submit it to a compute cluster. The following commands are specific to the
sciCORE compute environment at the university of Basel, adapt to your 
requirements.
 
create a stdout directory:

```bash
mkdir -p data/baselines/assembly_consensus/stdout
```

and submit with following script, adapt where needed:

```bash
#!/bin/bash
#SBATCH --job-name=one_vs_all
#SBATCH --qos=6hours
#SBATCH --output=<GIT_ROOT>/data/baselines/assembly_consensus/stdout/%A_%a.out
#SBATCH --mem=12G
#SBATCH --array=1-12293

ml Python/3.6.6-foss-2018b

$(head -$SLURM_ARRAY_TASK_ID <GIT_ROOT>/data/baselines/assembly_consensus/one_vs_all.cmd | tail -1)
```

After submission we have one file per model, for further processing we put that
in one nice JSON file:

```bash
./ema baseline_predictors/assembly_consensus/collect.py data/baselines/assembly_consensus data/baselines/assembly_consensus/assembly_consensus_scores.json
```


### TS Consensus, aka lDDT consensus

Create a directory to dump results:

```bash
mkdir -p data/baselines/ts_consensus
```

If you executed the data collection scripts collect_ts_models.py and
collect_ts_target_sequences.py as described above, you can directly
fire the analysis for some random target:

```bash
./ema baseline_predictors/ts_consensus/ts_consensus_baseline.py data/ts_target_sequences/T1159.fasta data/ts_models/T1159/ data/baselines/ts_consensus/T1159.json
```

A file with such commands for all targets can be derived with:

```bash
./ema baseline_predictors/ts_consensus/create_cmds.py <GIT_ROOT>/ema <GIT_ROOT>/baseline_predictors/ts_consensus/ts_consensus_baseline.py <GIT_ROOT>/data/ts_target_sequences/ <GIT_ROOT>/data/ts_models/ <GIT_ROOT>/data/baselines/ts_consensus/ <GIT_ROOT>/data/baselines/ts_consensus/run_all.cmd
```

Again, thats a submission script specific to sciCORE at the university of Basel.
Much more memory is requested this time:

```bash
#!/bin/bash
#SBATCH --job-name=lddt_consensus
#SBATCH --qos=6hours
#SBATCH --output=<GIT_ROOT>/data/baselines/ts_consensus/stdout/%A_%a.out
#SBATCH --mem=50G
#SBATCH --array=1-93

ml Python/3.6.6-foss-2018b

$(head -$SLURM_ARRAY_TASK_ID <GIT_ROOT>/data/baselines/ts_consensus/run_all.cmd | tail -1)

```

## Analysis

### Collect scores, i.e. target values

All values relevant for the global EMA analysis can be concatenated into a
csv file using the following command:

```bash
./ema analysis/collect_global_data.py assembly_targets.json data/usalign_results/ data/scores/ data/global_scores.csv
````

The file we just generated can be combined with ema predictions using:

```bash
./ema analysis/collect_ema_data_global.py ema_targets.json data/ema_predictions/ data/global_scores.csv data/assembly_models/ data/ema_global_scores.csv --assembly_consensus_out data/baselines/assembly_consensus/assembly_consensus_scores.json
```

Same two steps for local:

```bash
./ema analysis/collect_local_data.py assembly_targets.json data/usalign_results/ data/scores data/local_scores.csv
````

```bash
./ema analysis/collect_ema_data_local.py ema_targets.json data/ema_predictions/ data/local_scores.csv data/ema_local_scores.csv
```

The self-assessment analysis runs in the following two steps to process results for single-chain and assemblies respectively:

```bash
./ema analysis/collect_local_lddt_ts.py --ts_baseline_path data/baselines/ts_consensus data/ts_models data/ts_lddt_results data/self_assessment_local 1> collect_local_lddt_ts.out 2> collect_local_lddt_ts.err
```

```bash
./ema analysis/collect_local_lddt_oligos.py assembly_targets.json data/assembly_models data/scores data/self_assessment_local 1> collect_local_lddt_oligos.out 2> collect_local_lddt_oligos.err
```

The redirection of the output in the commands above is meant to check for issues in the output. It is expected for the stderr to have plenty of lines about residues containing unknown atom and duplicate atoms. In the stdout there is expected output starting with "SKIPPING" and "PROCESSING" for skipped and processed targets/models respectively. Any other line indicates an issue. The only known issues are 7 oligo models which were not scored. Otherwise, there should be no unexpected output in stdout.

The two collect scripts result in two json files which are processed with `analysis/analysis_ts.ipynb` and `analysis/analysis_oligo.ipynb` respectively.

Analysis is done in Jupyter notebooks which must be fired from <GIT_ROOT> with

```bash
./ema --notebook
```

The following notebooks can be opened from the browser:

* analysis/local_analysis.ipynb
* analysis/global_analysis.ipynb

The manuscript performs a specialized analysis for antibodies. It's a variant
of local analysis solely focusing on residues that are relevant for
antibody/antigen interactions. You need to create some intermediate data:

```bash
cd <GIT_ROOT>/analysis
../ema extract_epitope_interface_residues.py
```
which creates the file `epitope_interface_residues.json` in the `data`
directory. Once done, the following notebook performs the analysis:

* analysis/local_analysis_ab.ipynb

## Other stuff

### Benchmark your new EMA method 

You can hijack the CASP15 EMA analysis code and insert your own method as if it
participated itself! Checkout the Jupyter notebook in `custom_analysis`. There
you also find a csv file with preprocessed data for the global analysis
(SCORE/QSCORE). So you don't need any of the original data. If you have all
Python dependencies with recent versions (pandas, matplotlib, sklearn, numpy),
you don't even need the container setup.

### Compute lDDT on RNA

The following command computes lDDT without stereochemistry check on monomers.
Intended for experimental use on RNA. Stereo-chemistry checks might still be
added.

```bash
./ema scoring/monomer_lddt_no_stereocheck.py <MDL> <TRG> <OUT_JSON>
```

