import argparse
import json
import os
import time
import traceback

from ost import io
from ost.mol.alg import chain_mapping
from ost.mol.alg import qsscore

def _parse_args():

    desc = ("Computes one vs all chain mappings including accompanying "
            "QS-score. Expects all models, including the ref model, to reside "
            "in the same directory. The chain mapping is a speed vs accuracy "
            "tradeoff. See ChainMapper.GetRigidMapping in the OpenStructure "
            "docs for more information. The mapping is done with the "
            " \"greedy_iterative_rmsd\" strategy and subsampling enabled (50).")

    parser = argparse.ArgumentParser(description = desc)
    parser.add_argument("model_dir", help="path to directory containing models "
                        "in pdb format")
    parser.add_argument("ref_model", help="filename of model in model_dir which "
                        "is the reference")
    parser.add_argument("out", help="path to output file")

    return parser.parse_args() 

def main():
    args = _parse_args()

    model_files = os.listdir(args.model_dir)
    if args.ref_model not in model_files:
        raise RuntimeError(f"ref model file ({args.ref_model}) is not present "
                           f"in specified model dir {args.model_dir}")

    ref_model = io.LoadPDB(os.path.join(args.model_dir, args.ref_model))
    mapper = chain_mapping.ChainMapper(ref_model)

    out = dict()
    out["ref_model"] = args.ref_model
    out["chem_groups"] = mapper.chem_groups
    out["mappings"] = list()

    t0 = time.time()
    for mf in model_files:
        if mf == args.ref_model:
            continue
        try:
            model = io.LoadPDB(os.path.join(args.model_dir, mf))
            res = mapper.GetRigidMapping(model, strategy="greedy_iterative_rmsd",
                                         subsampling=50,
                                         single_chain_gdtts_thresh=0.0)
            qs_scorer = qsscore.QSScorer.FromMappingResult(res)
            qs_score_result = qs_scorer.Score(res.mapping)

            trg_pos = geom.Vec3List()
            mdl_pos = geom.Vec3List()
            n_trg_not_mapped = 0 # number of target res withough corresponding mdl res
            processed_trg_chains = set()

            # fill trg_pos and mdl_pos with CA positions from mapped residues
            for trg_ch, mdl_ch in res.GetFlatMapping().items():
                processed_trg_chains.add(trg_ch)
                aln = res.alns[(trg_ch, mdl_ch)]
                for col in aln:
                    if col[0] != '-' and col[1] != '-':
                        trg_res = col.GetResidue(0)
                        mdl_res = col.GetResidue(1)
                        trg_at = trg_res.FindAtom("CA")
                        mdl_at = mdl_res.FindAtom("CA")
                        if not trg_at.IsValid():
                            trg_at = trg_res.FindAtom("C3'")
                        if not mdl_at.IsValid():
                            mdl_at = mdl_res.FindAtom("C3'")
                        trg_pos.append(trg_at.GetPos())
                        mdl_pos.append(mdl_at.GetPos())
                    elif col[0] != '-':
                        n_trg_not_mapped += 1

            # count number of trg residues from non-mapped chains
            for ch in res.target.chains:
                if ch.GetName() not in processed_trg_chains:
                    n_trg_not_mapped += len(ch.residues)

            # perform simple superposition based on Kabsch algorithm
            try:
                # superposition can fail in very rare cases, namely when the required
                # backbone atoms are not there which leads to empty mdl_pos, trg_pos
                superposition_res = mol.alg.SuperposeSVD(mdl_pos, trg_pos)
                mdl_pos.ApplyTransform(superposition_res.transformation)
                n_within = trg_pos.GetGDTTS(mdl_pos, norm=False)
                oligo_gdtts = n_within / (4*len(trg_pos) + 4*n_trg_not_mapped)
            except:
                oligo_gdtts = None

            out["mappings"].append({"model": mf,
                                    "mapping": res.mapping,
                                    "qs_score": qs_score_result.QS_best,
                                    "gdtts": oligo_gdtts})
        except:
            print("failed for:", mf, " skip...")
            traceback.print_exc()

    with open(args.out, 'w') as fh:
        json.dump(out, fh)

    N = len(out["mappings"])
    print(f"computed {N} in {time.time() - t0} s")

if __name__ == '__main__':
    main()
