import argparse
import os

def _parse_args():

    desc = ("Generates a file with commands that execute "
            "one_vs_all_chain_mapping.py for each model. ")
    parser = argparse.ArgumentParser(description = desc)
    parser.add_argument("ema_exec", help="path to ema executable")
    parser.add_argument("one_vs_all_script", help="path to "
                        "one_vs_all_chain_mapping.py script")
    parser.add_argument("assembly_dir", help="Directory that contains "
                        "collected assemblies.")
    parser.add_argument("mapping_out_dir", help="Where all the result files "
                        "from one_vs_all_chain_mapping.py end up")
    parser.add_argument("out", help="outfile with created commands")
    return parser.parse_args() 

def main():
    args = _parse_args()
    commands = list()
    trg_dirs = os.listdir(args.assembly_dir)
    trg_dirs = [d for d in trg_dirs if os.path.isdir(os.path.join(args.assembly_dir, d))]
    for td in trg_dirs:
        mdl_files = os.listdir(os.path.join(args.assembly_dir, td))
        for mf in mdl_files:
            cmd = [args.ema_exec,
                   args.one_vs_all_script,
                   os.path.join(args.assembly_dir, td),
                   mf,
                   os.path.join(args.mapping_out_dir, mf + ".json")]
            commands.append(' '.join(cmd))
    with open(args.out, 'w') as fh:
        fh.write(os.linesep.join(commands))

if __name__ == '__main__':
    main()
