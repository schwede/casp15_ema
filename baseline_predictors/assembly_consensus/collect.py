import argparse
import json
import os
import os
import json

import numpy as np

def _parse_args():

    desc = ("Collects results from all one_vs_all_chain_mapping.py runs and "
            "computes average QS scores. Results are dumped in JSON format.")

    parser = argparse.ArgumentParser(description = desc)
    parser.add_argument("result_dir", help="Path to dir containing JSON files "
                        "for all mdls generated by one_vs_all_chain_mapping.py")
    parser.add_argument("out", help="output JSON file")

    return parser.parse_args() 

def main():
    args = _parse_args()

    files = [f for f in os.listdir(args.result_dir) if f.endswith(".json")]
    data = dict()
    for f in files:
        with open(os.path.join(args.result_dir, f), 'r') as fh:
            mdl_data = json.load(fh)
        mdl = f.split('.')[0]
        qs_scores = [x["qs_score"] for x in mdl_data["mappings"]]
        gdtts_scores = [x["gdtts"] for x in mdl_data["mappings"]]

        tmp = [x for x in qs_scores if x is not None]
        if len(tmp) < len(qs_scores):
            print(f"{f} None in QS scores:", len(qs_scores)-len(tmp))
        qs_scores = tmp

        tmp = [x for x in gdtts_scores if x is not None]
        if len(tmp) < len(gdtts_scores):
            print(f"{f} None in GDTTS scores:", len(gdtts_scores)-len(tmp))
        gdtts_scores = tmp

        data[mdl] = {"SCORE": np.mean(gdtts_scores),
                     "QSCORE": np.mean(qs_scores)}
    with open(args.out, 'w') as fh:
        json.dump(data, fh)

if __name__ == '__main__':
    main()
