import argparse
import os
import json
import numpy as np

def _parse_args():

    desc = ("Loads all models that refer to specified SEQRES and computes "
            "per-residue consensus score.")

    parser = argparse.ArgumentParser(description = desc)
    parser.add_argument("seqres", help="Fasta file with reference sequence")
    parser.add_argument("mdl_dir", help="A directory with models referring to "
                        "seqres")
    parser.add_argument("out", help="Results for full target will dumped in "
                        "JSON format to this location")
    return parser.parse_args() 

def _SEQRESAlignment(seqres, view):
    """ Returns pairwise alignment using residue numbers of given view
    
    There is no sanity check whether one letter codes match
    
    :param seqres: Reference sequence to which residues in view are aligned
    :type seqres: :class:`ost.seq.SequenceHandle`
    :param view: view from which ATOMSEQ will be extracted
    :type view: :class:`ost.mol.EntityView`/:class:`ost.mol.EntityHandle`
    :returns: Alignment with *seqres* as first sequence (gapless) and sequence
              from view as second sequence. The view is attached to second
              sequence.
    :raises: :class:`RuntimeError` if *view* has duplicate residue numbers or
             residue numbers that are not covered by *seqres*.
    """
    assert(len(view.chains) == 1)
    atomseq = ['-'] * seqres.GetLength()
    atomseq_name = view.chains[0].GetName()
    aligned_view = view.CreateEmptyView()
    for r in view.residues:
        rnum = r.GetNumber().GetNum()
        if rnum < 1 or rnum > len(atomseq):
            raise RuntimeError(f"Invalid resnum: {r.GetQualifiedName()}")
        if atomseq[rnum-1] != '-':
            raise RuntimeError(f"Duplicate resnum: {r.GetQualifiedName()}")
        atomseq[rnum-1] = r.one_letter_code
        aligned_view.AddResidue(r, mol.ViewAddFlag.INCLUDE_ALL)
    atomseq = ''.join(atomseq)
    aln = seq.CreateAlignment()
    aln.AddSequence(seqres)
    aln.AddSequence(seq.CreateSequence(atomseq_name, atomseq))
    aln.AttachView(1, aligned_view)
    return aln

def main():
    args = _parse_args()
    mdl_files = os.listdir(args.mdl_dir)
    seqres = io.LoadSequence(args.seqres)
    assert(len(seqres) > 0)
    aln_list = seq.AlignmentList()
    for mf in mdl_files:
        mdl = io.LoadPDB(os.path.join(args.mdl_dir, mf), fault_tolerant=True)
        mdl = mdl.Select("aname=CA")
        aln_list.append(_SEQRESAlignment(seqres, mdl))
    aln = seq.alg.MergePairwiseAlignments(aln_list, seqres)
    d_map = seq.alg.CreateDistanceMap(aln) 
    mean_lddt = seq.alg.CreateMeanlDDTHA(d_map)
    data = mean_lddt.GetData()
    result = dict()
    assert(len(data) == len(seqres))
    assert(len(aln_list) == len(mdl_files))
    assert(len(aln_list) == len(data[0]))
    result = dict()
    for idx, mf in enumerate(mdl_files):
        rnums = list()
        scores = list()
        aln = aln_list[idx]
        for col_idx, col in enumerate(aln):
            if col[1] != '-':
                r = col.GetResidue(1)
                rnums.append(r.GetNumber().GetNum())
                scores.append(data[rnums[-1]-1][idx])
        result[mf] = {"global": np.mean(scores),
                      "local": {n:s for n,s in zip(rnums, scores)}}
    
    with open(args.out, 'w') as fh:
        json.dump(result, fh)

if __name__ == '__main__':
    main()
