import os
import argparse

def _parse_args():
    desc = ("Creates commands to execute ts_consensus_baseline.py for each "
            "target for which we have a reference sequence")

    parser = argparse.ArgumentParser(description = desc)
    parser.add_argument("ema_exec", help="path to ema executable")
    parser.add_argument("ts_consensus_baseline_script", help="Path to script")
    parser.add_argument("seqres_dir", help="Path to directory that contains "
                        "all reference (SEQRES) sequences - thats what you get "
                        "when executing collect_ts_target_sequences.py")
    parser.add_argument("mdl_dir", help="A directory containing a directory "
                        "with models for each target - thats what you get when "
                        "executing collect_ts_models.py")
    parser.add_argument("out_dir", help="Directory in which output JSONs will "
                        "be written")
    parser.add_argument("out_file", help="outfile to write all commands")
    return parser.parse_args()

def main():
    args = _parse_args()
    seqres_files = os.listdir(args.seqres_dir)
    seqres_paths = list()
    mdl_paths = list()
    out_paths = list()
    for sf in seqres_files:
        t = sf.split(".")[0]
        mdl_path = os.path.join(args.mdl_dir, t)
        if not os.path.exists(mdl_path):
            raise RuntimeError(f"No models for {sf}")
        seqres_paths.append(os.path.join(args.seqres_dir, sf))
        mdl_paths.append(mdl_path)
        out_paths.append(os.path.join(args.out_dir, f"{t}.json"))

    commands = list()
    for s_p, mdl_p, o_p in zip(seqres_paths, mdl_paths, out_paths):
        cmd = [args.ema_exec,
               args.ts_consensus_baseline_script,
               s_p, mdl_p, o_p]
        commands.append(' '.join(cmd))

    with open(args.out_file, 'w') as fh:
        fh.write(os.linesep.join(commands))

if __name__ == '__main__':
    main()
