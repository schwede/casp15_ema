import json
import os

from ost import io

score_dir = "../data/scores"
trg_dir = "../data/assembly_targets"

mdl_dirs = ["../data/assembly_models/H1166",
            "../data/assembly_models/H1167",
            "../data/assembly_models/H1168"]

epitope_interface_residues = list()

def GetReprView(ent):
    """ Returns view with representative peptide atoms => CB, CA for GLY
    
    Ensures that each residue has exactly one atom with assertions
    
    :param ent: Entity for which you want the representative view
    :param ent: :class:`ost.mol.EntityHandle`/:class:`ost.mol.EntityView`
    :returns: :class:`ost.mol.EntityView` derived from ent
    """
    repr_ent = ent.Select("(aname=\"CB\" or (rname=\"GLY\" and aname=\"CA\"))")
    for r in repr_ent.residues:
        assert(len(r.atoms) == 1)
    return repr_ent

for mdl_dir in mdl_dirs:
    trg_name = os.path.basename(mdl_dir)
    mdl_files = os.listdir(mdl_dir)
    
    for mdl_file in mdl_files:
        mdl = io.LoadPDB(os.path.join(mdl_dir, mdl_file))
        with open(os.path.join(score_dir,  mdl_file + "_" + trg_name + ".json")) as fh:
            scoring_data = json.load(fh)

        if "interface_residues" not in scoring_data:
            print("skip", mdl_file)
            continue

        mapping = scoring_data["mapping"]
        repr_view = GetReprView(mdl)
        for rstring in scoring_data["interface_residues"]:
            cname = rstring.split('.')[0]
            rnum = mol.ResNum(int(rstring.split('.')[1]))
            r = repr_view.FindResidue(cname, rnum)
            close_atoms = repr_view.FindWithin(r.atoms[0].GetPos(), 8.0)
            trg_chain_pairs = list()
            for ca in close_atoms:
                trg_chain_pairs.append(sorted([mapping[r.GetChain().GetName()],
                                               mapping[ca.GetChain().GetName()]]))
            if ["A", "B"] not in trg_chain_pairs:
                epitope_interface_residues.append([mdl_file, rstring])

with open("../data/epitope_interface_residues.json", 'w') as fh:
    json.dump(epitope_interface_residues, fh)

print(len(epitope_interface_residues))

        

        




    

    


