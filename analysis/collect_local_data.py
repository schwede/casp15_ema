import argparse
import os
import json
import pandas as pd

def _parse_args():

    desc = ("Collects local scores and dumps them as csv file.")

    parser = argparse.ArgumentParser(description = desc)
    parser.add_argument("target_file", help="JSON file that relates targets "
                        "with actual target filenames. Sits in <GIT_ROOT>")
    parser.add_argument("mmalign_out_dir", help = "out_dir argument in "
                        "collect_mmalign_results.py (can be MMalign or USalign "
                        "results)")
    parser.add_argument("scores_dir", help="Directory with json result files "
                        "generated from scoring.py. Expects the following "
                        "naming: <mdl_name>_<trg_name>.json. "
                        "This is satisfied if you used the "
                        "create_scoring_cmds.py script for creating commands.")
    parser.add_argument("csv_out", help="path to csv out file")
    return parser.parse_args() 

def main():
    args = _parse_args()

    with open(args.target_file) as fh:
        target_data = json.load(fh)

    tmp = os.listdir(args.mmalign_out_dir)
    tmscore_result_files = [f for f in tmp if f.endswith(".SUMMARY")]
    
    # tmscores are stored with key [<mdl_file>, <trg_file>]
    tmscores = dict()
    for f in tmscore_result_files:
        if "T1127v2" in f:
            continue # dirty hack
        with open(os.path.join(args.mmalign_out_dir, f), 'r') as fh:
            data = fh.readlines()
        if "<!DOCTYPE HTML " in data[0]:
            continue
        for line in data:
            if len(line.strip()) > 0 and len(line.split()) > 1:
                mdl_file = line.split()[0]
                trg_file = line.split()[-1].split(':')[0]
                tmscore = float(line.split()[1])
                tmscores[(mdl_file, trg_file)] = tmscore

    df_data = {"mdl": list(),
               "trg": list(),
               "cname": list(),
               "rnum": list(),
               "patch_qs": list(),
               "patch_dockq": list(),
               "lddt": list(),
               "cad": list(),
               "global_qs_best": list(),
               "global_tm_score": list(),
               "n_mdl_chains": list(),
               "true_interface_residue": list()}

    tmp = os.listdir(args.scores_dir)
    json_files = [f for f in tmp if f.endswith(".json")]
    for jf in json_files:
        with open(os.path.join(args.scores_dir, jf), 'r') as fh:
            data = json.load(fh)
        if "errors" in data and len(data["errors"]) > 0:
            print("skip json file with errors:", jf)
            continue

        global_tm_score = None
        mdl = os.path.split(data["mdl_file"])[1]
        trg = os.path.split(data["trg_file"])[1]
        tm_key = (mdl, trg)
        if tm_key in tmscores:
            global_tm_score = tmscores[tm_key]
        elif tm_key[0][:5] == "H1171":
            tm_key = (f"H1171v{tm_key[1][6]}{tm_key[0][5:]}", tm_key[1])
            if tm_key in tmscores:
                global_tm_score = tmscores[tm_key]
        elif tm_key[0][:5] == "H1172":
            tm_key = (f"H1172v{tm_key[1][6]}{tm_key[0][5:]}", tm_key[1])
            if tm_key in tmscores:
                global_tm_score = tmscores[tm_key]

        unique_chains = set()
        for k in data["local_lddt"].keys():
            unique_chains.add(k.split('.')[0])
        n_chains = len(unique_chains)

        trg_interface_residues = data["target_interface_residues"]

        for int_res, patch_qs, patch_dockq in zip(data["interface_residues"], data["patch_qs"], data["patch_dockq"]):
            cname = int_res.split('.')[0]
            trg_cname = None
            if cname in data["mapping"]:
                trg_cname = data["mapping"][cname]
            rnum = int(int_res.split('.')[1])
            df_data["mdl"].append(mdl)
            df_data["trg"].append(trg)
            df_data["cname"].append(cname)
            df_data["rnum"].append(rnum)
            df_data["patch_qs"].append(patch_qs)
            df_data["patch_dockq"].append(patch_dockq)
            df_data["lddt"].append(data["local_lddt"][int_res])
            df_data["cad"].append(data["local_cad_score"][int_res])
            df_data["global_qs_best"].append(data["qs_best"])
            df_data["global_tm_score"] = global_tm_score
            df_data["n_mdl_chains"].append(n_chains)
            df_data["true_interface_residue"].append(f"{trg_cname}.{rnum}." in trg_interface_residues)

    df = pd.DataFrame.from_dict(df_data)
    df.to_csv(args.csv_out)

if __name__ == '__main__':
    main()
