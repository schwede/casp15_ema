import argparse
import os
import json
import pandas as pd

def _parse_args():

    desc = ("Collects global scores from two data sources: 1) MMalign/USalign "
            "results that have been fetched with collect_mmalign_results.py ,"
            "2) global scores computed with scoring.py. "
            "The collected scores are dumped as csv file.")

    parser = argparse.ArgumentParser(description = desc)
    parser.add_argument("target_file", help="JSON file that relates targets "
                        "with actual target filenames. Sits in <GIT_ROOT>")
    parser.add_argument("mmalign_out_dir", help = "out_dir argument in "
                        "collect_mmalign_results.py (can be MMalign or USalign "
                        "results)")
    parser.add_argument("scores_dir", help="Directory with json result "
                        "files generated from scoring.py. Expects "
                        "the following naming: <mdl_name>_<trg_name>.json. "
                        "This is satisfied if you used the "
                        "create_scoring_cmds.py script for creating "
                        "commands.")
    parser.add_argument("csv_out", help="path to csv out file")
    return parser.parse_args() 

def main():
    args = _parse_args()

    with open(args.target_file) as fh:
        target_data = json.load(fh)

    tmp = os.listdir(args.mmalign_out_dir)
    tmscore_result_files = [f for f in tmp if f.endswith(".SUMMARY")]
    
    # tmscores are stored with key [<mdl_file>, <trg_file>]
    tmscores = dict()
    for f in tmscore_result_files:
        if "T1127v2" in f:
            continue # dirty hack
        with open(os.path.join(args.mmalign_out_dir, f), 'r') as fh:
            data = fh.readlines()
        if "<!DOCTYPE HTML " in data[0]:
            continue
        for line in data:
            if len(line.strip()) > 0 and len(line.split()) > 1:
                mdl_file = line.split()[0]
                trg_file = line.split()[-1].split(':')[0]
                tmscore = float(line.split()[1])
                tmscores[(mdl_file, trg_file)] = tmscore

    df_data = {"mdl": list(),
               "trg": list(),
               "qs_global": list(),
               "qs_best": list(),
               "gdtts": list(),
               "lddt": list(),
               "rmsd": list(),
               "dockq_ave": list(),
               "dockq_wave": list(),
               "tm_score": list(),
               "n_mdl_chains": list(),
               "n_mdl_residues": list(),
               "chain_mapping": list()}

    processed_tm_scores = set()

    tmp = os.listdir(args.scores_dir)
    json_files = [f for f in tmp if f.endswith(".json")]
    for jf in json_files:
        with open(os.path.join(args.scores_dir, jf), 'r') as fh:
            data = json.load(fh)
        if "errors" in data and len(data["errors"]) > 0:
            print("skip json file with errors:", jf)
            continue
        df_data["mdl"].append(os.path.split(data["mdl_file"])[1])
        df_data["trg"].append(os.path.split(data["trg_file"])[1])
        df_data["qs_global"].append(data["qs_global"])
        df_data["qs_best"].append(data["qs_best"])
        df_data["gdtts"].append(data["gdtts"])
        df_data["lddt"].append(data["lddt"])
        df_data["rmsd"].append(data["rmsd"])
        df_data["dockq_ave"].append(data["dockq_ave"])
        df_data["dockq_wave"].append(data["dockq_wave"])
        tm_key = (df_data["mdl"][-1], df_data["trg"][-1])
        if tm_key in tmscores:
            df_data["tm_score"].append(tmscores[tm_key])
        elif tm_key[0][:5] == "H1171":
            tm_key = (f"H1171v{tm_key[1][6]}{tm_key[0][5:]}", tm_key[1])
            if tm_key in tmscores:
                df_data["tm_score"].append(tmscores[tm_key])
        elif tm_key[0][:5] == "H1172":
            tm_key = (f"H1172v{tm_key[1][6]}{tm_key[0][5:]}", tm_key[1])
            if tm_key in tmscores:
                df_data["tm_score"].append(tmscores[tm_key])
        else:
            df_data["tm_score"].append(None)
        unique_chains = set()
        for k in data["local_lddt"].keys():
            unique_chains.add(k.split('.')[0])
        df_data["n_mdl_chains"].append(len(unique_chains))
        df_data["n_mdl_residues"].append(len(data["local_lddt"]))
        df_data["chain_mapping"].append(json.dumps(data["mapping"]))
        processed_tm_scores.add(tm_key)

    print("There would be the following mdl/trg with TMscores but without "
          "global scores:")
    for tm_key, tm_score in tmscores.items():
        if tm_key not in processed_tm_scores:
            print(tm_key)

    df = pd.DataFrame.from_dict(df_data)
    df.to_csv(args.csv_out)

if __name__ == '__main__':
    main()
