import argparse
import os
import numpy as np
import json
from ost import io

def _parse_args():

    desc = ("Map local lDDT predictions from oligo models (obtained via "
            "collect_assemblies) to lDDT evaluations (obtained via "
            "scoring) and produce a json file ready for analysis "
            "of self-assessment. The specified out_dir folder will contain a "
            "json file for each target and oligo_lddt_processed.json for all "
            "targets combined.")

    parser = argparse.ArgumentParser(description = desc)
    parser.add_argument("target_file", help="JSON file that relates targets "
                        "with actual target filenames. Sits in <GIT_ROOT>")
    parser.add_argument("mdl_base_path", help="Base path containing target "
                        "folders with models (obtained via collect_assemblies)")
    parser.add_argument("scores_dir", help="Directory with json result "
                        "files generated from scoring.py following the same "
                        "naming scheme. Used to extract lDDT values.")
    parser.add_argument("out_dir", help="Will contain a json file for each "
                        "target and oligo_lddt_processed.json for all targets "
                        "combined.")
    parser.add_argument("--skip_if_done", default=False, action="store_true",
                        help="Reuse existing per-target json files instead "
                        "of recollecting the data.")
    return parser.parse_args()


def _get_class(rnum_str, asa_rel, int_res, surface_ASA_threshold):
    # return value: 0 = core, 1 = surface, 2 = interface
    # -> logic: first check for core then split rest according to interface residues
    rASA = asa_rel[rnum_str]
    # rASA = [rASA in complex, rASE as monomer]
    if rASA[1] <= surface_ASA_threshold:
        return 0
    elif int(rnum_str) in int_res:
        return 2
    else:
        return 1

def _process_model(pdb_path, scores_path, surface_ASA_threshold=25):
    """Process a single model.
    
    Returns dictionary with:
    - matched_lddts_ref & matched_lddts_mdl: same sized vectors with local
      lDDTs from scores_path (ref) and pdb_path (mdl)
    - matched_res_nums: with res nums for res. in matched_lddts_...
    - chain_info: dict with mdl_ch_name, ref_ch_name, first_idx, last_idx to map
      chains into matched_res_nums (use [first_idx:last_idx+1] for slicing)
    - matched_ref/mdl_class: 0 = core, 1 = surface, 2 = interface in ref/mdl
    - stereocheck_atoms_removed_mdl: num. atoms removed from model
    - num_non_matched: number of residues in mdl which were not evaluated
      (i.e. missing in ref)
    - errors: list of errors found

    Notes:
    - models with failed scoring have only errors filled!
    - surface_ASA_threshold taken from SM
    """
    # load files
    ent = io.LoadPDB(pdb_path, fault_tolerant=True)
    json_data = json.load(open(scores_path))
    # check errors
    if "errors" in json_data:
        errors = json_data["errors"]
    else:
        # only there if errors happened
        errors = []
    if "local_lddt" not in json_data:
        print(f"LDDT FAILED IN SCORES FILE. Errors: {errors}")
        return {"errors": errors}
    else:
        # fetch dict (ch_name: str(res_num): value (or None))
        local_lddt = json_data["local_lddt"]
    # chain mapping
    mdl_ref_ch_map = json_data["mapping"]
    # collect data
    stereocheck_atoms_removed_mdl = json_data.get("stereocheck_atoms_removed_mdl")
    matched_values = [] # tuples: (ref, mdl, res_num, ref_class, mdl_class)
    num_non_matched = 0
    chain_infos = [] # dict with chain names and first/last idx into matched_values
    for ch in ent.chains:
        ch_name = ch.name
        if ch_name in local_lddt:
            ch_lddt = local_lddt[ch_name]
        else:
            print(f"MISSING MODEL CHAIN {ch_name} IN SCORES")
            continue
        # skip ones with no scored residues at all
        if all(v is None for v in ch_lddt.values()):
            num_non_matched += ch.residue_count
            continue
        # get chain info
        ref_ch_name = mdl_ref_ch_map[ch_name]
        chain_info = {"mdl_ch_name": ch_name,
                      "ref_ch_name": ref_ch_name,
                      "first_idx": len(matched_values)}
        # prep info on classes
        mdl_int_res = set(json_data["interface_residues"][ch_name])
        ref_int_res = set(json_data["trg_interface_residues"][ref_ch_name])
        # note: .._asa_rel[rnum_str] = [rASA in complex, rASE as monomer]
        mdl_asa_rel = json_data["mdl_asa_rel"][ch_name]
        ref_asa_rel = json_data["trg_asa_rel"][ref_ch_name]
        # parse residues
        for res in ch.residues:
            rnum_str = str(res.number)
            if rnum_str in ch_lddt:
                ref_lddt = ch_lddt[rnum_str]
                if ref_lddt:
                    # note on rounding: PDB b-factors have 2 digit accuracy
                    # and scores file has lDDT in [0,1] at 3 digit accuracy
                    ref_lddt = round(ref_lddt * 100, 2)
                    mdl_lddt = round(np.mean([a.b_factor for a in res.atoms]), 2)
                    # get classes
                    ref_class = _get_class(rnum_str, ref_asa_rel, ref_int_res,
                                           surface_ASA_threshold)
                    mdl_class = _get_class(rnum_str, mdl_asa_rel, mdl_int_res,
                                           surface_ASA_threshold)
                    matched_values.append((ref_lddt, mdl_lddt, int(rnum_str),
                                           ref_class, mdl_class))
                else:
                    num_non_matched += 1
            else:
                print(f"MISSING MODEL RESIDUE {ch_name}.{rnum_str} IN SCORES")
                num_non_matched += 1
        chain_info["last_idx"] = len(matched_values) - 1
        chain_infos.append(chain_info)
    # output
    json_data = {
        "matched_lddts_ref": [v[0] for v in matched_values],
        "matched_lddts_mdl": [v[1] for v in matched_values],
        "matched_res_nums": [v[2] for v in matched_values],
        "chain_infos": chain_infos,
        "matched_ref_class": [v[3] for v in matched_values],
        "matched_mdl_class": [v[4] for v in matched_values],
        "stereocheck_atoms_removed_mdl": stereocheck_atoms_removed_mdl,
        "num_non_matched": num_non_matched,
        "errors": errors
    }
    return json_data


def main():
    args = _parse_args()

    if not os.path.exists(args.out_dir):
        os.makedirs(args.out_dir)

    # check all files
    target_data = json.load(open(args.target_file))
    trg_names = sorted(os.listdir(args.mdl_base_path))
    score_files = sorted(os.listdir(args.scores_dir))
    # check if any unexpected ones in target_data?
    only_in_trg_data = set(target_data) - set(trg_names)
    if only_in_trg_data:
        print(f"UNEXPECTED MISMATCH IN TRG DIRS VS TRG DATA {only_in_trg_data}")
    # check available targets (and skip ones to skip)
    score_files_test = set(score_files)
    trg_names_todo = []
    for trg_name in trg_names:
        # known ones to skip (anything including "v")
        if "v" in trg_name:
            print("SKIPPING", trg_name)
            continue
        # check names
        if trg_name not in target_data:
            print(f"UNKNOWN TARGET {trg_name}")
            continue
        trg_names_todo.append(trg_name)
        # only first target file evaluated
        # -> score file name is [MDLNAME]_[TRG_BASE].json
        trg_file = target_data[trg_name][0]
        trg_base = os.path.splitext(trg_file)[0]
        # check files
        mdl_path = os.path.join(args.mdl_base_path, trg_name)
        mdl_files = sorted(os.listdir(mdl_path))
        scores_check = [f"{mf}_{trg_base}.json" for mf in mdl_files]
        only_score = set(scores_check) - score_files_test
        if only_score:
            print("NON SCORED MODELS:", sorted(only_score))
        score_files_test -= set(scores_check)
        # NOTE: expected to have plenty of score files left in score_files_test
        #       at the end (no worries)

    # process all of them
    all_data = []
    for trg_name in trg_names_todo:
        # check if done already
        json_path = os.path.join(args.out_dir, f"oligo_{trg_name}.json")
        if os.path.exists(json_path) and args.skip_if_done:
            print(f"FETCHING PRECOMPUTED SCORES FOR {trg_name}")
            all_data.extend(json.load(open(json_path)))
            continue
        # go through models
        trg_data = []
        mdl_path = os.path.join(args.mdl_base_path, trg_name)
        mdl_names = sorted(os.listdir(mdl_path))
        trg_file = target_data[trg_name][0]
        trg_base = os.path.splitext(trg_file)[0]
        for mdl_name in mdl_names:
            pdb_file = os.path.join(mdl_path, mdl_name)
            score_file = f"{mdl_name}_{trg_base}.json"
            if score_file in score_files:
                # process it
                print("PROCESSING", trg_name, mdl_name)
                pdb_path = os.path.join(mdl_path, mdl_name)
                scores_path = os.path.join(args.scores_dir, score_file)
                trg_name_check, group_mdl = mdl_name.split("TS")
                if trg_name_check not in trg_name:
                    print("WEIRD FILE NAME", mdl_name)
                group_id, mdl_id = group_mdl.split("_")[:2]
                # NOTE: some models suffixed with "o" (=> cut mdl_id)
                data = {
                    "trg_name": trg_name,
                    "group_id": group_id,
                    "mdl_id": mdl_id[0],
                }
                try:
                    data.update(_process_model(pdb_path, scores_path))
                    data["failed"] = "matched_lddts_ref" not in data
                except Exception as ex:
                    import traceback
                    traceback.print_exc()
                    data["failed"] = True
                trg_data.append(data)
            else:
                print("MISSING SCORES", trg_name, mdl_name)
        # keep copy per target
        json.dump(trg_data, open(json_path, "w"),
                  separators=(',', ':'))
        all_data.extend(trg_data)

    # dump it all
    json_out = os.path.join(args.out_dir, "oligo_lddt_processed.json")
    json.dump(all_data, open(json_out, "w"),
              separators=(',', ':'))


if __name__ == '__main__':
    main()
