import argparse
import os
import json
import re
import pandas as pd

def _parse_args():

    desc = ("Loads predictions that have been collected with "
            "collect_ema_predictions.py and combines them with the csv output "
            "from collect_local_data.py to prepare a csv ready for final "
            "analysis of local prediction accuracy.")

    parser = argparse.ArgumentParser(description = desc)
    parser.add_argument("target_file", help="ema specific JSON file that "
                        "relates targets with actual target filenames. Sits "
                        "in <GIT_ROOT>")
    parser.add_argument("ema_predictions", help = "out_dir argument in "
                        "collect_ema_predictions.py")
    parser.add_argument("local_scores", help="csv file generated with "
                        "collect_local_data.py")
    parser.add_argument("csv_out", help="path to csv out file")
    return parser.parse_args() 

def main():
    args = _parse_args()

    with open(args.target_file) as fh:
        target_data = json.load(fh)

    local_scores_df = pd.read_csv(args.local_scores)

    # figure out what ema groups we have based on the ema prediction filenames
    ema_groups = set()
    ema_target_dirs = os.listdir(args.ema_predictions)
    for d in ema_target_dirs:
        prediction_files = os.listdir(os.path.join(args.ema_predictions, d))
        for f in prediction_files:
            ema_groups.add(f.split("QA")[1].split("_")[0])

    print("ema groups:", ema_groups)

    # prepare score dict for them
    # organized as: score_dict[group][(mdl_f, cname, rnum)] = score
    score_dict = {g: dict() for g in ema_groups}

    # regular expression to parse per-residue scores
    # matches stuff like "A123:0.42"
    regex = "([a-zA-Z])(\d+):([+-]?([0-9]*[.])?[0-9]+)"

    for d in ema_target_dirs:
        print("process ema dir", d)
        try:
            trg_file = target_data[d][0]
        except:
            # homo-oligomers get a 'o' attached: T1187 => T1187o
            # thats missing from the directory names
            trg_file = target_data[d + 'o'][0]
        for f in os.listdir(os.path.join(args.ema_predictions, d)):
            group = f.split("QA")[1].split("_")[0]
            full_path = os.path.join(args.ema_predictions, d, f)
            with open(full_path, 'r') as fh:
                data = fh.readlines()
            active_mdl = None
            for line in data:
                if line.startswith("END"):
                    break
                split_line = line.split()
                if line.startswith(d):
                    if len(line.split()) < 3:
                        raise RuntimeError("Invalid format: ", full_path)
                    active_mdl = line.split()[0]
                    split_line = split_line[3:]
                if active_mdl is None:
                    continue
                for item in split_line:
                    match = re.fullmatch(regex, item)
                    if match:
                        groups = match.groups()
                        cname = groups[0]
                        rnum = int(groups[1])
                        pred = float(groups[2])
                    else:
                        if group == "120" and trg_file == "T1109o.pdb" and \
                        line.startswith("T1110"):
                            continue # that guy owes me a beer
                        raise RuntimeError(f"Cannot parse local score item: "
                                           f"{item} in {full_path}")
                    score_dict[group][(active_mdl, cname, rnum)] = pred

    # group 248 used zero based indexing instead of residue numbers. Lets add
    # another group with corrected residue numbers, yet another beer!
    score_dict["248_2"] = {(k[0], k[1], k[2]+1):v for k,v in score_dict["248"].items()}
    ema_groups.add("248_2")

    print("parsed data points per group:")
    for g in ema_groups:
        print(g, len(score_dict[g]))

    df_data = {"mdl": list(),
               "trg": list(),
               "cname": list(),
               "rnum": list(),
               "patch_qs": list(),
               "patch_dockq": list(),
               "lddt": list(),
               "cad": list(),
               "global_qs_best": list(),
               "global_tm_score": list(),
               "n_mdl_chains": list(),
               "true_interface_residue": list()}

    for g in ema_groups:
        df_data.update({g: list()})

    for target, target_files in target_data.items():
        trg_file = target_files[0]
        sub_df = local_scores_df[local_scores_df["trg"] == trg_file]
        
        df_data["mdl"].extend(sub_df["mdl"])
        df_data["trg"].extend(sub_df["trg"])
        df_data["cname"].extend(sub_df["cname"])
        df_data["rnum"].extend(sub_df["rnum"]) 
        df_data["patch_qs"].extend(sub_df["patch_qs"]) 
        df_data["patch_dockq"].extend(sub_df["patch_dockq"]) 
        df_data["lddt"].extend(sub_df["lddt"]) 
        df_data["cad"].extend(sub_df["cad"])
        df_data["global_qs_best"].extend(sub_df["global_qs_best"]) 
        df_data["global_tm_score"].extend(sub_df["global_tm_score"])
        df_data["n_mdl_chains"].extend(sub_df["n_mdl_chains"])
        df_data["true_interface_residue"].extend(sub_df["true_interface_residue"])

        for g in ema_groups:
            scores = list()
            for mdl, trg, cname, rnum in zip(sub_df["mdl"], sub_df["trg"],
                                             sub_df["cname"], sub_df["rnum"]):
                key = (mdl, cname, rnum)
                if key in score_dict[g]:
                    scores.append(score_dict[g][key])
                else:
                    scores.append(None)
            df_data[g].extend(scores)

    df = pd.DataFrame.from_dict(df_data)
    df.to_csv(args.csv_out)

if __name__ == '__main__':
    main()
