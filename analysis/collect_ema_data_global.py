import argparse
import os
import json
import pandas as pd

def _parse_args():

    desc = ("Loads predictions that have been collected with "
            "collect_ema_predictions.py and combines them with the csv output "
            "from collect_global_data.py to prepare a csv ready for final "
            "analysis of global prediction accuracy.")

    parser = argparse.ArgumentParser(description = desc)
    parser.add_argument("target_file", help="ema specific JSON file that "
                        "relates targets with actual target filenames. Sits "
                        "in <GIT_ROOT>")
    parser.add_argument("ema_predictions", help = "out_dir argument in "
                        "collect_ema_predictions.py")
    parser.add_argument("global_scores", help="csv file generated with "
                        "collect_global_data.py")
    parser.add_argument("model_dir", help="Directory containing a directory "
                        "with models for each target.")
    parser.add_argument("csv_out", help="path to csv out file")
    parser.add_argument("--assembly_consensus_out", help="If given, additional "
                        "data for assembly consensus (AC) will be added to "
                        "output csv", default=None)
    return parser.parse_args() 

def main():
    args = _parse_args()

    with open(args.target_file) as fh:
        target_data = json.load(fh)

    global_scores_df = pd.read_csv(args.global_scores)

    # construct lists of trg and mdl structures as a reference frame for all the
    # scores/predictions
    targets = list()
    models = list()
    for trg, trg_files in target_data.items():
        mdl_dir = os.path.join(args.model_dir, trg)
        tmp = list(os.listdir(mdl_dir))
        targets.extend([trg_files[0] for x in tmp])
        models.extend(tmp)

    # Fetch scores from global_scores_df and relate it to underlying files
    qs_best_dict = dict()
    for mdl_f, trg_f, s in zip(global_scores_df["mdl"], global_scores_df["trg"],
                               global_scores_df["qs_best"]):
        qs_best_dict[(mdl_f, trg_f)] = s

    dockq_wave_dict = dict()
    for mdl_f, trg_f, s in zip(global_scores_df["mdl"], global_scores_df["trg"],
                               global_scores_df["dockq_wave"]):
        dockq_wave_dict[(mdl_f, trg_f)] = s

    tmscore_dict = dict()
    for mdl_f, trg_f, s in zip(global_scores_df["mdl"], global_scores_df["trg"],
                               global_scores_df["tm_score"]):
        tmscore_dict[(mdl_f, trg_f)] = s

    gdtts_dict = dict()
    for mdl_f, trg_f, s in zip(global_scores_df["mdl"], global_scores_df["trg"],
                               global_scores_df["gdtts"]):
        gdtts_dict[(mdl_f, trg_f)] = s

    n_mdl_chains_dict = dict()
    for mdl_f, trg_f, n in zip(global_scores_df["mdl"], global_scores_df["trg"],
                               global_scores_df["n_mdl_chains"]):
        n_mdl_chains_dict[(mdl_f, trg_f)] = n

    # figure out what ema groups we have based on the ema prediction filenames
    ema_groups = set()
    ema_target_dirs = os.listdir(args.ema_predictions)
    for d in ema_target_dirs:
        prediction_files = os.listdir(os.path.join(args.ema_predictions, d))
        for f in prediction_files:
            ema_groups.add(f.split("QA")[1].split("_")[0])

    # prepare score dict for them
    # organized as: SCORE_dict[group][(mdl_f, trg_f)] = SCORE
    SCORE_dict = {g: dict() for g in ema_groups}
    QSCORE_dict = {g: dict() for g in ema_groups}

    for d in ema_target_dirs:
        try:
            trg_file = target_data[d][0]
        except:
            # homo-oligomers get a 'o' attached: T1187 => T1187o
            # thats missing from the directory names
            trg_file = target_data[d + 'o'][0]
        for f in os.listdir(os.path.join(args.ema_predictions, d)):
            group = f.split("QA")[1].split("_")[0]
            full_path = os.path.join(args.ema_predictions, d, f)
            with open(full_path, 'r') as fh:
                data = fh.readlines()
            for line in data:
                if line.startswith(d):
                    if len(line.split()) < 3:
                        raise RuntimeError("Invalid format: ", full_path)
                    mdl_file = line.split()[0]
                    SCORE = line.split()[1].strip()
                    QSCORE = line.split()[2].strip()

                    if SCORE == 'X':
                        SCORE = None
                    else:
                        SCORE = float(SCORE)

                    if QSCORE == 'X':
                        QSCORE = None
                    else:
                        QSCORE = float(QSCORE)

                    SCORE_dict[group][(mdl_file, trg_file)] = SCORE
                    QSCORE_dict[group][(mdl_file, trg_file)] = QSCORE

    df_data = {"mdl": list(),
               "trg": list(),
               "qs_best": list(),
               "dockq_wave": list(),
               "tm_score": list(),
               "gdtts": list(),
               "n_mdl_chains": list()}
    for g in ema_groups:
        df_data.update({g + "_SCORE": list(), g + "_QSCORE": list()})

    for t, m in zip(targets, models):
        df_data["mdl"].append(m)
        df_data["trg"].append(t)
        key = (m, t)

        if key in qs_best_dict:
            df_data["qs_best"].append(qs_best_dict[key])
        else:
            df_data["qs_best"].append(None)

        if key in dockq_wave_dict:
            df_data["dockq_wave"].append(dockq_wave_dict[key])
        else:
            df_data["dockq_wave"].append(None)

        if key in tmscore_dict:
            df_data["tm_score"].append(tmscore_dict[key])
        else:
            df_data["tm_score"].append(None)

        if key in gdtts_dict:
            df_data["gdtts"].append(gdtts_dict[key])
        else:
            df_data["gdtts"].append(None)

        if key in n_mdl_chains_dict:
            df_data["n_mdl_chains"].append(n_mdl_chains_dict[key])
        else:
            df_data["n_mdl_chains"].append(None)

        for g in ema_groups:
            if key in SCORE_dict[g]:
                df_data[g + "_SCORE"].append(SCORE_dict[g][key])
            else:
                df_data[g + "_SCORE"].append(None)

            if key in QSCORE_dict[g]:
                df_data[g + "_QSCORE"].append(QSCORE_dict[g][key])
            else:
                df_data[g + "_QSCORE"].append(None)

    if args.assembly_consensus_out is not None:
        with open(args.assembly_consensus_out, 'r') as fh:
            ac_data = json.load(fh)
        df_data["AC_SCORE"] = list()
        df_data["AC_QSCORE"] = list()
        for m in models:
            if m in ac_data:
                df_data["AC_SCORE"].append(ac_data[m]["SCORE"])
                df_data["AC_QSCORE"].append(ac_data[m]["QSCORE"])
            else:
                df_data["AC_SCORE"].append(None)
                df_data["AC_QSCORE"].append(None)

    df = pd.DataFrame.from_dict(df_data)
    df.to_csv(args.csv_out)

if __name__ == '__main__':
    main()
