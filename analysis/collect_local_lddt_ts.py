import argparse
import os
import numpy as np
import json
from ost import io

def _parse_args():

    desc = ("Map local lDDT predictions from single-chain models (obtained via "
            "collect_ts_models) to lDDT evaluations (obtained via "
            "collect_lddt_results) and produce a json file ready for analysis "
            "of self-assessment. The specified out_dir folder will contain a "
            "json file for each target and ts_lddt_processed.json for all "
            "targets combined.")

    parser = argparse.ArgumentParser(description = desc)
    parser.add_argument("mdl_base_path", help="Base path containing target "
                        "folders with models (obtained via collect_ts_models)")
    parser.add_argument("lddt_base_path", help="Base path containing target "
                        "folders with .lddt files (obtained via "
                        "collect_lddt_results).")
    parser.add_argument("out_dir", help="Will contain a json file for each "
                        "target and ts_lddt_processed.json for all targets "
                        "combined.")
    parser.add_argument("--ts_baseline_path", help="Path containing one json "
                        "per target with pred. local lDDT scores from a "
                        "baseline predictor (e.g. ts_consensus_baseline).",
                        default="", type=str, metavar="<BASELINE DIR>")
    parser.add_argument("--skip_if_done", default=False, action="store_true",
                        help="Reuse existing per-target json files instead "
                        "of recollecting the data.")
    return parser.parse_args()


def _parse_lddt_file(lddt_path):
    """Parse lddt file and return dict keyed on residue numbers.
    
    Items in returned dict:
    - rname (str): three letter code for residue (for sanity checks)
    - has_q_prob (bool): True if Q. problems in model residue
    - score (float): local lDDT in [0, 100]
    """
    # assumptions:
    # - single chain (i.e. can ignore chain name)
    # - each residue number appears only once
    # - res. missing in ref. have "Asses." == "No"
    # - res. with stereochemistry/clash issues have 
    #   "Asses." == "Yes" and a valid score (reduced according to issues)
    # - lDDT runs w/o stereochemistry checks get a warning but
    #   otherwise all processed with has_q_prob = False
    table_reached = False
    has_q_prob_column = False
    res_dict = {}
    with open(lddt_path) as f_in:
        for line in f_in:
            lsplit = line.split('\t')
            if table_reached:
                if len(lsplit) < 5:
                    break
                if has_q_prob_column:
                    ch, rname, rnum_str, assessed, q_prob, score_str = lsplit[:6]
                else:
                    ch, rname, rnum_str, assessed, score_str = lsplit[:5]
                    q_prob = "No"
                if assessed == "Yes":
                    rnum = int(rnum_str)
                    if q_prob == "No":
                        has_q_prob = False
                    else:
                        if q_prob not in ["Yes", "Yes+"]:
                            print("WEIRD LDDT LINE (Q.Prob.)", line.strip())
                        has_q_prob = True
                    # note on rounding: score_str has 4 digits accuracy
                    score = round(float(score_str) * 100, 2)
                    if rnum in res_dict:
                        print("WEIRD LDDT LINE (dupl. rnum)", line.strip())
                    else:
                        res_dict[rnum] = {
                            "res_name": rname,
                            "has_q_prob": has_q_prob,
                            "score": score,
                        }
                elif assessed != "No":
                    print("WEIRD LDDT LINE (Asses.)", line.strip())
            elif lsplit[:6] == ["Chain", "ResName", "ResNum", "Asses.", "Q.Prob.", "Score"]:
                table_reached = True
                has_q_prob_column = True
            elif lsplit[:5] == ["Chain", "ResName", "ResNum", "Asses.", "Score"]:
                table_reached = True
                has_q_prob_column = False
    if not table_reached:
        print("WEIRD: lDDT TABLE NOT FOUND!!")
    return res_dict

def _process_model(pdb_file, lddt_file):
    """Process a single model.
    
    Returns dictionary with:
    - matched_lddts_ref & matched_lddts_mdl: same sized vectors with local
      lDDTs from lddt_file (ref) and pdb_file (mdl)
    - matched_res_nums: with res nums for res. in matched_lddts_...
    - q_prob_indices: indices into matched_lddts_.. for mdl residues
      with stereochemistry/clash issues
    - num_non_matched: number of residues in mdl which were not evaluated
      (i.e. missing in ref)
    """
    # load data
    ent = io.LoadPDB(pdb_file, fault_tolerant=True)
    lddt_res_dict = _parse_lddt_file(lddt_file)
    if ent.chain_count != 1:
        print("WEIRD CHAIN COUNT", [(ch.name, ch.residue_count) for ch in ent.chains])
    # collect data
    matched_lddts = [] # tuples: (ref, mdl, res_num)
    q_prob_indices = [] # indices into matched_lddts for ones with stereochemistry/clash issues
    num_non_matched = 0
    for res in ent.residues:
        if res.number.num in lddt_res_dict:
            ref_data = lddt_res_dict[res.number.num]
            if res.name != ref_data["res_name"]:
                print("RES. NAME MISMATCH", res.qualified_name, ref_data["res_name"])
                num_non_matched += 1
            else:
                ref_lddt = ref_data["score"]
                # note on rounding: PDB b-factors have 2 digit accuracy
                mdl_lddt = round(np.mean([a.b_factor for a in res.atoms]), 2)
                if ref_data["has_q_prob"]:
                    q_prob_indices.append(len(matched_lddts))
                matched_lddts.append((ref_lddt, mdl_lddt, res.number.num))
        else:
            num_non_matched += 1
    # output
    json_data = {
        "matched_lddts_ref": [v[0] for v in matched_lddts],
        "matched_lddts_mdl": [v[1] for v in matched_lddts],
        "matched_res_nums": [v[2] for v in matched_lddts],
        "q_prob_indices": q_prob_indices,
        "num_non_matched": num_non_matched,
    }
    return json_data

def main():
    # HC settings
    # targets to skip as they were canceled 
    canceled_trgs = ["T1105", "T1118", "T1186", "T1185s3", "T1189", "T1190",
                     "T1191", "T1192", "T1193"]
    # targets where domain-based lDDT used
    need_suffix = {"T1120": "-D12", "T1170": "-D12"}

    args = _parse_args()

    if not os.path.exists(args.out_dir):
        os.makedirs(args.out_dir)

    # go through all model targets
    trg_names = sorted(os.listdir(args.mdl_base_path))
    # check if any unexpected ones in lDDT?
    trg_names_lddt = os.listdir(args.lddt_base_path)
    exp_trg_names_lddt = [trg_name + need_suffix.get(trg_name, "") \
                          for trg_name in trg_names]
    only_in_lddt = sorted(set(trg_names_lddt) - set(exp_trg_names_lddt))
    if only_in_lddt:
        print("UNEXPECTED EXTRA FOLDERS WITH LDDT RESULTS", only_in_lddt)
    # check available targets
    trg_names_todo = []
    for trg_name in trg_names:
        # known ones to skip (anything else than T.. and any ..vX and T1105)
        if not trg_name.startswith("T") or trg_name[-2] == "v" \
           or trg_name in canceled_trgs:
            print("SKIPPING", trg_name)
            continue
        # does it have results?
        lddt_suffix = need_suffix.get(trg_name, "")
        lddt_path = os.path.join(args.lddt_base_path, trg_name + lddt_suffix)
        if os.path.exists(lddt_path):
            trg_names_todo.append(trg_name)
            # check files
            mdl_path = os.path.join(args.mdl_base_path, trg_name)
            mdl_files = sorted(os.listdir(mdl_path))
            lddt_files = sorted(os.listdir(lddt_path))
            lddt_check = [os.path.splitext(f)[0] for f in lddt_files \
                          if f.endswith(".lddt")]
            if lddt_suffix:
                lddt_check = [f.split(lddt_suffix)[0] for f in lddt_check]
            if lddt_check != mdl_files:
                print(f"TRG {trg_name} MISMATCHING FILES")
                only_lddt = sorted(set(lddt_check) - set(mdl_files))
                if only_lddt:
                    print("-> ONLY LDDT FILES", only_lddt)
                only_mdl = sorted(set(mdl_files) - set(lddt_check))
                if only_mdl:
                    print("-> ONLY MDL FILES", only_mdl)
            # check baseline scores
            if args.ts_baseline_path:
                baseline_json_path = os.path.join(args.ts_baseline_path,
                                                  trg_name + ".json")
                if os.path.exists(baseline_json_path):
                    baseline_data = json.load(open(baseline_json_path))
                    mdls_base = sorted(baseline_data.keys())
                    if mdls_base != mdl_files:
                        print(f"TRG {trg_name} MISMATCHING BASELINE FILES")
                        only_base = sorted(set(mdls_base) - set(mdl_files))
                        if only_base:
                            print("-> ONLY IN BASELINE", only_lddt)
                        only_mdl = sorted(set(mdl_files) - set(mdls_base))
                        if only_mdl:
                            print("-> ONLY MDL FILES", only_mdl)
                else:
                    print(f"TRG {trg_name} LACKING BASELINE RESULTS")
        else:
            print(f"TRG {trg_name} LACKING RESULTS")

    # process all of them
    all_data = []
    for trg_name in trg_names_todo:
        # check if done already
        json_path = os.path.join(args.out_dir, f"ts_{trg_name}.json")
        if os.path.exists(json_path) and args.skip_if_done:
            print(f"FETCHING PRECOMPUTED SCORES FOR {trg_name}")
            all_data.extend(json.load(open(json_path)))
            continue
        # skip ones with lacking baseline scores if baseline scores expected
        if args.ts_baseline_path:
            baseline_json_path = os.path.join(args.ts_baseline_path,
                                              trg_name + ".json")
            if not os.path.exists(baseline_json_path):
                continue
        # go through models
        trg_data = []
        lddt_suffix = need_suffix.get(trg_name, "")
        lddt_path = os.path.join(args.lddt_base_path, trg_name + lddt_suffix)
        mdl_path = os.path.join(args.mdl_base_path, trg_name)
        mdl_names = sorted(os.listdir(mdl_path))
        lddt_names = set(os.listdir(lddt_path))
        for mdl_name in mdl_names:
            pdb_file = os.path.join(mdl_path, mdl_name)
            lddt_name = mdl_name + lddt_suffix + ".lddt"
            if lddt_name in lddt_names:
                # process it
                print("PROCESSING", trg_name, mdl_name)
                pdb_file = os.path.join(mdl_path, mdl_name)
                lddt_file = os.path.join(lddt_path, lddt_name)
                trg_name_check, group_mdl = mdl_name.split("TS")
                if trg_name_check != trg_name:
                    print("WEIRD FILE NAME", mdl_name)
                group_id, mdl_id = group_mdl.split("_")
                data = {
                    "trg_name": trg_name,
                    "group_id": group_id,
                    "mdl_id": mdl_id,
                    "mdl_name": mdl_name
                }
                try:
                    data.update(_process_model(pdb_file, lddt_file))
                    data["failed"] = False
                except Exception as ex:
                    import traceback
                    traceback.print_exc()
                    data["failed"] = True
                trg_data.append(data)
            else:
                print("MISSING LDDT", trg_name, mdl_name)
        # add baseline scores
        if args.ts_baseline_path:
            baseline_data = json.load(open(baseline_json_path))
            for data in trg_data:
                mdl_name = data["mdl_name"]
                if mdl_name in baseline_data:
                    local_lddt_base = baseline_data[mdl_name]["local"]
                    matched_lddts_base = [
                        round(local_lddt_base[str(res_num)] * 100, 2) \
                        for res_num in data["matched_res_nums"]
                    ]
                    data["matched_lddts_base"] = matched_lddts_base
        # keep copy per target
        json.dump(trg_data, open(json_path, "w"),
                  separators=(',', ':'))
        all_data.extend(trg_data)
                
    # dump it all
    json_out = os.path.join(args.out_dir, "ts_lddt_processed.json")
    json.dump(all_data, open(json_out, "w"),
              separators=(',', ':'))


if __name__ == '__main__':
    main()
